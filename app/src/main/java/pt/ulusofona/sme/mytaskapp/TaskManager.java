package pt.ulusofona.sme.mytaskapp;

import java.util.ArrayList;
import java.util.List;

public class TaskManager {

    private static TaskManager _instance;

    private ArrayList<String> tasklist;

    private void initTaskList() {
        this.tasklist.add("Ver email");
        this.tasklist.add("Ver vídeos SME");
        this.tasklist.add("Implementar projeto SME");
        this.tasklist.add("Gravar Story no Instagram");
        this.tasklist.add("Entregar trabalho SME");
        this.tasklist.add("Tweetar sobre Lusófona");
        this.tasklist.add("Jogar PES");
        this.tasklist.add("Comprar pão");
        this.tasklist.add("Comprar prenda Natal pais");
        this.tasklist.add("Comprar prenda Natal avós");
    }

    private TaskManager() {
        this.tasklist = new ArrayList<String>();
        initTaskList();
    }

    public static TaskManager getInstance() {
        if (_instance == null) {
            _instance = new TaskManager();
        }
        return _instance;
    }


    public List<String> getTasks() {
        return tasklist;
    }

    public void addTask(String taskName) {
        this.tasklist.add(taskName);
    }


    public void deleteTask(String taskName) {
        this.tasklist.remove(taskName);
    }



}
