package pt.ulusofona.sme.mytaskapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AddTaskActivity extends AppCompatActivity {

    private EditText addTaskNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        addTaskNameEditText = this.findViewById(R.id.addTaskTextInputEditText);
    }

    public void actionAddTask(View view) {
        String taskName = addTaskNameEditText.getText().toString();
        TaskManager.getInstance().addTask(taskName);

        this.finish();
    }
}
